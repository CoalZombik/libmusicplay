﻿using System;
using System.Threading;

namespace libmusicplay
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			OptimalPlay();
			return;
		}

		public static void OptimalPlay ()
		{
			//Print library version
			Console.WriteLine(MusicPlay.Version);

			//Init musicplay
			MusicPlay play = new MusicPlay();

			//Select backend
			if (MusicPlay.CheckBackend(MusicPlay.Backend.PulseAudio))
				play.InitBackend(MusicPlay.Backend.PulseAudio);
			else if (MusicPlay.CheckBackend(MusicPlay.Backend.Jack))
				play.InitBackend(MusicPlay.Backend.Jack);
			else if (MusicPlay.CheckBackend(MusicPlay.Backend.Alsa))
				play.InitBackend(MusicPlay.Backend.Alsa);
			else if (MusicPlay.CheckBackend(MusicPlay.Backend.Wasapi))
				play.InitBackend(MusicPlay.Backend.Wasapi);
			else if (MusicPlay.CheckBackend(MusicPlay.Backend.CoreAudio))
				play.InitBackend(MusicPlay.Backend.CoreAudio);
			else
				play.InitBackend(MusicPlay.Backend.Dummy);

			//Get info about all devices
			MusicPlay.DeviceInfo[] devices = play.GetDevices();

			//Retrieve info about the default output device
			MusicPlay.DeviceInfo defaultDevice = play.GetDefaultOutputDevice ();

			//Print all devices to select one
			foreach (MusicPlay.DeviceInfo device in devices)
			{
				bool isDefault = device.Equals (defaultDevice);

				Console.WriteLine("{0}) {1}{2}", device.Index, device.Name, isDefault ? " (default)" : "");
				Console.WriteLine("\tIs RAW: {0}", device.IsRaw);
				Console.WriteLine("\tSoftware latency: {0} - {1}", device.SoftwareLatencyMin, device.SoftwareLatencyMax);
				Console.WriteLine("\tSample rate: {0} - {1}", device.SampleRateMin, device.SampleRateMax);
			}

			string readed = "";

			//User select device
			Console.Write("Select device: ");
			readed = Console.ReadLine();
			int selected;
			if (!int.TryParse(readed, out selected) || selected < 0 || selected >= devices.Length)
			{
				Console.WriteLine("Invalid input!");
				return;
			}

			//Get output device info for stream config
			MusicPlay.DeviceInfo deviceInfo = devices[selected];

			//User select sample rate
			Console.Write("Select sample rate ({0} - {1}): ", deviceInfo.SampleRateMin, deviceInfo.SampleRateMax);
			readed = Console.ReadLine();
			int samplerate;
			if (!int.TryParse(readed, out samplerate) || samplerate < deviceInfo.SampleRateMin || samplerate > deviceInfo.SampleRateMax)
			{
				Console.WriteLine("Invalid input!");
				return;
			}

			//User select latency
			Console.Write("Select latency ({0} - {1}): ", deviceInfo.SoftwareLatencyMin, deviceInfo.SoftwareLatencyMax);
			readed = Console.ReadLine();
			float latency;
			if (!float.TryParse(readed, out latency) || (deviceInfo.SoftwareLatencyMax > 0 && (latency < deviceInfo.SoftwareLatencyMin || latency > deviceInfo.SoftwareLatencyMax)))
			{
				Console.WriteLine("Invalid input!");
				return;
			}


			//Init output device
			play.InitDevice(deviceInfo);

			//Init stream with specified samplerate and latency
			play.InitStream("sample_play (libmusicplay example)", samplerate, latency);

			//Load audio, select SincFastest and play
			play.LoadAudio("music_orig.wav", MusicPlay.ResampleQuality.SincFastest);
			play.Play();

			//Wait to end of file and write current audio time
			while (!play.EndOfFile)
			{
				Console.WriteLine(play.CurrentAudioTime.ToString("#0.000"));
				Thread.Sleep(250);
			}

			//Dispose class
			play.Dispose();
		}
	}
}
