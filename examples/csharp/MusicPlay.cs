﻿using System;
using System.Threading;
using System.Runtime.InteropServices;

namespace libmusicplay
{
	public class MusicPlay : IDisposable
	{
		#region Native enums and structures
		public enum Backend
		{
			None = 0,
			Jack,
			PulseAudio,
			Alsa,
			CoreAudio,
			Wasapi,
			Dummy,
		}

		/// <summary>
		/// Quality of audio resampling
		/// </summary>
		public enum ResampleQuality : int
		{
			SincBest = 0,
			SincMedium,
			SincFastest,
			ZeroOrderHold,
			Linear
		}

		internal struct DeviceInfoInternal
		{
#pragma warning disable 0649
			public int index;
			public IntPtr id;
			public IntPtr name;
			public bool is_raw;
			public double software_latency_min;
			public double software_latency_max;
			public int samplerate_min;
			public int samplerate_max;
#pragma warning restore 0649

			public DeviceInfoInternal (IntPtr ptr)
			{
				DeviceInfoInternal deviceInfoInternal = new DeviceInfoInternal();
				Marshal.PtrToStructure(ptr, deviceInfoInternal);
				this = deviceInfoInternal;
			}
		}
		#endregion

		#region Managed variables
		/// <summary>
		/// Info about device.
		/// </summary>
		public struct DeviceInfo
		{
			public int Index { get; private set; }
			public string Id { get; private set; }
			public string Name { get; private set; }
			public bool IsRaw { get; private set; }
			public double SoftwareLatencyMin { get; private set; }
			public double SoftwareLatencyMax { get; private set; }
			public int SampleRateMin { get; private set; }
			public int SampleRateMax { get; private set; }

			internal DeviceInfo (DeviceInfoInternal device)
			{
				Index = device.index;
				Id = Marshal.PtrToStringAuto(device.id);
				Name = Marshal.PtrToStringAuto(device.name);
				IsRaw = device.is_raw;
				SoftwareLatencyMin = device.software_latency_min;
				SoftwareLatencyMax = device.software_latency_max;
				SampleRateMin = device.samplerate_min;
				SampleRateMax = device.samplerate_max;
			}
		}

		IntPtr data = IntPtr.Zero;
		IntPtr error = IntPtr.Zero;

		/// <summary>
		/// Version of used libmusicplay library.
		/// </summary>
		public static string Version { get { return Marshal.PtrToStringAuto(musicplay_version(IntPtr.Zero, IntPtr.Zero, IntPtr.Zero)); } }

		/// <summary>
		/// Current audio time of playing file in seconds.
		/// </summary>
		public double CurrentAudioTime { get { double audiotime = musicplay_current_audio_time(data, error); CheckUnmanagedError(); return audiotime; } }

		/// <summary>
		/// Total audio time of playing file in seconds.
		/// </summary>
		/// <value>The total audio time.</value>
		public double TotalAudioTime { get { double audiotime = musicplay_total_audio_time(data, error); CheckUnmanagedError(); return audiotime; } }

		public bool EndOfFile { get { bool end = musicplay_end_of_file(data, error); CheckUnmanagedError(); return end; } }
		#endregion

		public MusicPlay ()
		{
			error = Marshal.AllocHGlobal(sizeof(int));
			data = musicplay_init(error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Check native library error
		/// </summary>
		void CheckUnmanagedError ()
		{
			int merror = Marshal.ReadInt32(error);
			if (merror != 0)
				throw new Exception(Marshal.PtrToStringAuto(musicplay_strerror(merror)));
		}

		public void Dispose ()
		{
			if (data != IntPtr.Zero)
			{
				musicplay_destroy(data);
				data = IntPtr.Zero;
			}
			if (error != IntPtr.Zero)
			{
				Marshal.FreeHGlobal(error);
				data = IntPtr.Zero;
			}
		}

		~MusicPlay ()
		{
			Dispose();
		}

		/// <summary>
		/// Check if selected backend is supported (compiled in libsoundio).
		/// </summary>
		/// <returns><c>true</c>, if backend is supported, <c>false</c> otherwise.</returns>
		public static bool CheckBackend (Backend backend) => musicplay_check_backend(backend);

		/// <summary>
		/// Init selected <paramref name="backend"/>. If failed, switch to dummy.
		/// This function must be runned before any manipulation with MusicPlay.
		/// </summary>
		/// <param name="backend">Selected backend</param>
		public void InitBackend (Backend backend)
		{
			musicplay_init_backend(data, backend, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Get all devices in selected backend.
		/// </summary>
		/// <returns>Array of info about devices</returns>
		public DeviceInfo[] GetDevices ()
		{
			int count = musicplay_get_device_count(data, error);
			CheckUnmanagedError();

			DeviceInfo[] output = new DeviceInfo[count];
			for (int i = 0; i < count; i++)
			{
				DeviceInfoInternal oneinternal = musicplay_get_device_info(data, i, error);
				CheckUnmanagedError();
				output[i] = new DeviceInfo(oneinternal);
				musicplay_free_device_info(oneinternal);
			}
			return output;
		}

		/// <summary>
		/// Get info about default output device in selected backend.
		/// </summary>
		/// <returns>Info about default output device</returns>
		public DeviceInfo GetDefaultOutputDevice () => GetDevices()[musicplay_get_default_output_device_index(data)];

		/// <summary>
		/// Set device do MusicPlayData for further usage (init stream etc.).
		/// </summary>
		public void InitDevice (DeviceInfo info) => InitDevice(info.Index);

		/// <summary>
		/// Set device for further usage (init stream etc.).
		/// </summary>
		/// <param name="index">Index of device</param>
		public void InitDevice (int index)
		{
			musicplay_init_device(data, index, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Open stream
		/// </summary>
		/// <param name="name">Name of application (for system info)</param>
		/// <param name="samplerate">Sample rate of stream</param>
		/// <param name="software_latency">Software latency of stream</param>
		public void InitStream (string name, int samplerate, double software_latency)
		{
			musicplay_init_stream(data, name, samplerate, software_latency, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Load audio
		/// </summary>
		/// <param name="filename">Path of audio file</param>
		/// <param name="quality">Quality of resampling</param>
		public void LoadAudio (string filename, ResampleQuality quality)
		{
			musicplay_load_audio(data, filename, quality, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Play audio
		/// </summary>
		public void Play ()
		{
			musicplay_play(data, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Pause audio
		/// </summary>
		/// <param name="pause">If set to <c>true</c> pause, otherwise unpause.</param>
		public void Pause (bool pause)
		{
			musicplay_pause(data, pause, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Wait in this thread to end of file.
		/// </summary>
		public void WaitToEndOfFile ()
		{
			while (!EndOfFile)
				Thread.Sleep(100);
		}

		/// <summary>
		/// Seek stream to specified time.
		/// This function is experimental.
		/// </summary>
		/// <param name="time">Time of stream</param>
		public void Seek (double time)
		{
			musicplay_seek(data, time, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Sync stream according to time.
		/// This function is experimental.
		/// </summary>
		/// <param name="time">Time of stream</param>
		public void Sync (double time)
		{
			musicplay_sync(data, time, error);
			CheckUnmanagedError();
		}

		/// <summary>
		/// Unload audio from memory.
		/// Run this function isn't necessary to free native memory.
		/// </summary>
		public void UnloadAudio () => musicplay_unload_audio(data);

		/// <summary>
		/// Close stream.
		/// Run this function isn't necessary to free native memory.
		/// </summary>
		public void DestroyStream () => musicplay_destroy_stream(data);

		/// <summary>
		/// Unconnect device.
		/// Run this function isn't necessary to free native memory.
		/// </summary>
		public void DestroyDevice () => musicplay_destroy_device(data);

		#region Native functions
		[DllImport("musicplay")]
		//[return: MarshalAs(UnmanagedType.LPStr)]
		static extern IntPtr musicplay_version (IntPtr major, IntPtr minor, IntPtr patch);

		[DllImport("musicplay")]
		//[return: MarshalAs(UnmanagedType.LPStr)]
		static extern IntPtr musicplay_strerror (int error);

		[DllImport("musicplay")]
		static extern IntPtr musicplay_init (IntPtr error);

		[DllImport("musicplay")]
		static extern bool musicplay_check_backend (Backend backend);

		[DllImport("musicplay")]
		static extern IntPtr musicplay_init_backend (IntPtr musicplay, Backend backend, IntPtr error);

		[DllImport("musicplay")]
		static extern int musicplay_get_device_count (IntPtr musicplay, IntPtr error);

		[DllImport("musicplay")]
		static extern int musicplay_get_default_output_device_index (IntPtr musicplay);

		[DllImport("musicplay")]
		static extern DeviceInfoInternal musicplay_get_device_info (IntPtr musicplay, int index, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_free_device_info (DeviceInfoInternal info);

		[DllImport("musicplay")]
		static extern void musicplay_init_device (IntPtr musicplay, int index, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_init_stream (IntPtr musicplay, [MarshalAs(UnmanagedType.LPStr)] string name, int samplerate, double software_latency, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_load_audio (IntPtr musicplay, [MarshalAs(UnmanagedType.LPStr)] string filename, ResampleQuality converter_type, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_play (IntPtr musicplay, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_pause (IntPtr musicplay, bool pause, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_seek (IntPtr musicplay, double time, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_sync (IntPtr musicplay, double time, IntPtr error);

		[DllImport("musicplay")]
		static extern double musicplay_current_audio_time (IntPtr musicplay, IntPtr error);

		[DllImport("musicplay")]
		static extern double musicplay_total_audio_time (IntPtr musicplay, IntPtr error);

		[DllImport("musicplay")]
		static extern bool musicplay_end_of_file (IntPtr musicplay, IntPtr error);

		[DllImport("musicplay")]
		static extern void musicplay_unload_audio (IntPtr musicplay);

		[DllImport("musicplay")]
		static extern void musicplay_destroy_stream (IntPtr musicplay);

		[DllImport("musicplay")]
		static extern void musicplay_destroy_device (IntPtr musicplay);

		[DllImport("musicplay")]
		static extern void musicplay_destroy (IntPtr musicplay);
		#endregion
	}
}
