#ifndef __AUDIO_PROCESS_H__
#define __AUDIO_PROCESS_H__

#include <soundio.h>
#include "types.h"

const char* musicplay_decoder_strerror(int error);

int get_pcm_and_resample (MusicPlayStreamData* data);

void stream_write (struct SoundIoOutStream* stream, int frame_count_min, int frame_count_max);

void stream_underflow (struct SoundIoOutStream* stream);

void stream_error (struct SoundIoOutStream* stream, int error);

convert_format_callback get_convert_callback (struct SoundIoDevice* device, enum SoundIoFormat* format, int* error);

decoder_init_callback get_decoder_init_callback (const char* filename);

decoder_destroy_callback get_decoder_destroy_callback (MusicPlayDecoderType type);

void samplerate_init (MusicPlayData* data, int converter_type, int* error);

#endif