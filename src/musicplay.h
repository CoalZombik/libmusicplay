#ifndef __MUSICPLAY_H__
#define __MUSICPLAY_H__

#include "types.h"

//Return library version.
const char* musicplay_version (int* major, int* minor, int* patch);

//Return human-readable info about error
const char* musicplay_strerror (int error);

//Alloc MusicPlayData and init soundio.
MusicPlayData* musicplay_init (int* error);

//Check if selected backend is supported (compiled in libsoundio).
bool musicplay_check_backend (enum SoundIoBackend backend);

//Init selected backend. If failed, switch to dummy.
void musicplay_init_backend (MusicPlayData* data, enum SoundIoBackend backend, int* error);

//Return device count in selected backend
int musicplay_get_device_count (MusicPlayData* data, int* error);

//Return the index of the default output device, or -1 if there are no devices
int musicplay_get_default_output_device_index (MusicPlayData* data);

//Get device info from selected device. Device info must be deallocated via musicplay_free_device_info().
MusicPlayDeviceInfo musicplay_get_device_info (MusicPlayData* data, int index, int* error);

//Free allocated memory from device info.
void musicplay_free_device_info (MusicPlayDeviceInfo info);

//Set device to MusicPlayData for further usage (init stream etc.).
void musicplay_init_device (MusicPlayData* data, int index, int* error);

//Open stream in device selected via musicplay_init_device().
void musicplay_init_stream (MusicPlayData* data, const char* name, int samplerate, double software_latency, int* error);

//Load audio from filename and setup decoder and resampler.
void musicplay_load_audio (MusicPlayData* data, const char* filename, int converter_type, int* error);

//Start playing loaded audio.
void musicplay_play (MusicPlayData* data, int* error);

//Pause stream.
void musicplay_pause (MusicPlayData* data, bool pause, int* error);

//Seek stream to specified time (experimental).
void musicplay_seek (MusicPlayData* data, double time, int* error);

//Sync stream according to time (experimental).
void musicplay_sync (MusicPlayData* data, double time, int* error);

//Return current playing time of loaded audio. It can be larger than total audio time.
double musicplay_current_audio_time (MusicPlayData* data, int* error);

//Return total time of audio
double musicplay_total_audio_time (MusicPlayData* data, int* error);

//Return true if whole audio file was played
bool musicplay_end_of_file (MusicPlayData* data, int* error);

//Free audio memory and destroy decoder and resampler.
void musicplay_unload_audio (MusicPlayData* data);

//Destroy stream. musicplay_unload_audio() is runned if it is necessary.
void musicplay_destroy_stream (MusicPlayData* data);

//Destroy device. musicplay_destroy_stream() is runned if it is necessary.
void musicplay_destroy_device (MusicPlayData* data);

//Destroy whole MusicPlayData with all components.
void musicplay_destroy (MusicPlayData* data);

#endif