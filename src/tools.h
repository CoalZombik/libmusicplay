#ifndef __MUSICPLAY_TOOLS_H__
#define __MUSICPLAY_TOOLS_H__

#include <stdlib.h>
#include <stdint.h>
#include "config.h"

#define SWAP_16(x) (((x) << 8 & 0xFF00) | \
					((x) >> 8 & 0x00FF))
#define SWAP_32(x) (((x) << 24 & 0xFF000000) | \
					((x) << 08 & 0x00FF0000) | \
					((x) >> 08 & 0x0000FF00) | \
					((x) >> 24 & 0x000000FF))
#define SWAP_64(x) (((x) << 56 & 0xFF00000000000000) | \
					((x) << 40 & 0x00FF000000000000) | \
					((x) << 24 & 0x0000FF0000000000) | \
					((x) << 08 & 0x000000FF00000000) | \
					((x) >> 08 & 0x00000000FF000000) | \
					((x) >> 24 & 0x0000000000FF0000) | \
					((x) >> 40 & 0x000000000000FF00) | \
					((x) >> 56 & 0x00000000000000FF))

#if IS_BIG_ENDIAN

#define BIG_ENDIAN_16(x) (x)
#define BIG_ENDIAN_32(x) (x)
#define BIG_ENDIAN_64(x) (x)

#define LITTLE_ENDIAN_16(x) SWAP_16(x)
#define LITTLE_ENDIAN_32(x) SWAP_32(x)
#define LITTLE_ENDIAN_64(x) SWAP_64(x)

#else

#define LITTLE_ENDIAN_16(x) (x)
#define LITTLE_ENDIAN_32(x) (x)
#define LITTLE_ENDIAN_64(x) (x)

#define BIG_ENDIAN_16(x) SWAP_16(x)
#define BIG_ENDIAN_32(x) SWAP_32(x)
#define BIG_ENDIAN_64(x) SWAP_64(x)

#endif

//Return monotonic time in milliseconds.
uint64_t monotonic_time ();

//Alloc dynamic memory and copy str.
char* duplicate_string (const char* str);

//Check if memory is not NULL. If it is NULL, return false.
bool check_prepared (void* memory, int* error);

//Load whole file to memory.
char* load_whole_file (const char* filename, size_t* size, int* error);

#endif