#ifndef __DECODER_OPUS_H__
#define __DECODER_OPUS_H__

#include "../types.h"

#define OPUS_EXTENSION "opus"

const char* decoder_opus_strerror (int error);

void decoder_opus_destroy (MusicPlayStreamData* data);

long decoder_opus_get_pcm (void* decoder, float* buffer, long buffer_frames, int* error);

void decoder_opus_seek (void* decoder, long frame, int* error);

void decoder_opus_init (MusicPlayStreamData* data, int stream_samplerate, int* error);

#endif