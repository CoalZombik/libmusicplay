#include <stdlib.h>
#include <opusfile.h>
#include "../types.h"

#define OPUS_SAMPLERATE 48000
#define OPUS_BUFFER_LENGTH (120)
#define OPUS_BUFFER_SIZE (OPUS_SAMPLERATE * OPUS_BUFFER_LENGTH / 1000)

const char* decoder_opus_strerror (int error)
{
	return "libopusfile error (see http://opus-codec.org/docs/opusfile_api-0.7/group__error__codes.html)";
}

void decoder_opus_destroy (MusicPlayStreamData* data)
{
	data->get_buffer_pcm = NULL;
	data->audio_seek = NULL;

	free(data->buffer);
	data->buffer_frames = 0;
	free(data->pcm);
	data->pcm_frames = 0;
	data->pcm_start = 0;
	data->pcm_left = 0;

	data->total_audio_frames = 0;
	data->frames_decoded = 0;

	op_free(data->decoder);
}

long decoder_opus_get_pcm (void* decoder, float* buffer, long buffer_frames, int* error)
{
	*error = MusicPlayNoError;

	int samples = op_read_float_stereo(decoder, buffer, buffer_frames * 2);
	if(samples < 0)
	{
		*error = OPUS_ERROR_START - samples;
		return 0;
	}

	return samples;
}

void decoder_opus_seek (void* decoder, long frame, int* error)
{
	int opus_error = op_pcm_seek(decoder, frame);
	if(opus_error != 0)
		*error = OPUS_ERROR_START - opus_error;
	else
		*error = MusicPlayNoError;
}

void decoder_opus_init (MusicPlayStreamData* data, int stream_samplerate, int* error)
{
	*error = MusicPlayNoError;
	if(data->filedata == NULL)
	{
		*error = MusicPlayNotPrepared;
		return;
	}

	data->samplerate = OPUS_SAMPLERATE;
	data->resample_ratio = (double)stream_samplerate / (double)OPUS_SAMPLERATE;

	data->buffer_frames = OPUS_BUFFER_SIZE;
	data->buffer = malloc(sizeof(float) * data->buffer_frames * 2);
	if(data->buffer == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		return;
	}

	data->pcm_frames = stream_samplerate * OPUS_BUFFER_LENGTH / 1000;
	data->pcm_start = 0;
	data->pcm_left = 0;
	data->pcm = malloc(sizeof(float) * data->pcm_frames * 2);
	if(data->pcm == NULL)
	{
		*error = MusicPlayInvalidAlloc;
		free(data->buffer);
		return;
	}

	int opus_error;
	data->decoder_type = MusicPlayOpus;
	data->decoder = op_open_memory(data->filedata, data->filesize, &opus_error);
	if(opus_error != 0)
	{
		*error = OPUS_ERROR_START - opus_error; //opus has negative error numbers
		free(data->buffer);
		free(data->pcm);
		return;
	}

	data->frames_played = 0;
	data->end_of_file = false;

	data->total_audio_frames = op_pcm_total(data->decoder, -1);
	data->frames_decoded = 0;

	data->get_buffer_pcm = decoder_opus_get_pcm;
	data->audio_seek = decoder_opus_seek;
}